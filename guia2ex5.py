palabra1 = len(input("ingrese la primera palabra:"))#se saca el tamaño con la funcion len
palabra2 = len(input("ingrese la segunda palabra:"))

resultado = palabra1 - palabra2 #se restan

if resultado < 0: #si el resultado es menor que 0 se multiplica por -1
    print("la segunda palabra es más grande por", resultado*-1,"letras")
elif resultado > 0: #sino, se muestra tal cual
    print("la primera palabra es más grande por", resultado, "letras")
else:#en caso de que sea 0, significa que son del mismo tamaño
    print("las palabras son del mismo tamaño")